<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="2mIH15vEkJCahcF1OTO2SGWpubuNPLh-kOVHHtBje6g" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home Emergency Cover Protection Plans | Smart Cover Insurance</title>
     <link rel="icon" href="https://smart-cover.co.uk/wp-content/uploads/sites/6/2017/05/cropped-Fsvicon.png" type="image/gif"> 
    

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.css" rel="stylesheet">
    <link href="assets/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

   <script src="assets/jquery.validate.min.js"></script>
   <script src="assets/custom.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174900159-1"></script>    
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-174900159-1');
    </script>

<!-- Hotjar Tracking Code for http://smartcover247.co.uk/he-quote-zone/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1818100,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- TrustBox script -->
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
<!-- End TrustBox script -->
  </head>
  <body> 
    
<div id="floating_menu" data-float="float-scroll" class="mff mfu">
   <header class="hbc" style="background-image:none; background-color:#ffffff">
      <div class="wrp side_logo clearfix has_phone" id="head_wrp">
         <div class="h-i">
            <div id="logo" class="left">
               <a class="lg" href="https://smart-cover.co.uk/">
               <img src="images/logo.png" alt="smart-cover.co.uk">
               </a>
            </div>
          
            <div class="mhl" id="nav_right" style="display: block; max-height: 492px;">

               <div class="phone_mobile navy">
                  <a href="tel:03333 449 559">
                     <div class="phr">
                        <span class="mphr">Call Us</span>
                        <span class="apnr">03333 449 559</span>
                     </div>
                  </a>
               </div>

               <nav class="right custom-right hidden-xs hidden-sm">
                   <ul class="menu custom-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="https://smart-cover.co.uk/">Home</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="https://smart-cover.co.uk/about-us">About Us</a>
                        </li>
                   </ul>
               </nav>

               <div class="clear"></div>
            </div>
            <div class="phone">
               <a href="tel:03333 449 559">
                  <div class="phr">
                     <span class="fphr">Call Us</span>
                     <span class="apnr">03333 449 559</span>
                  </div>
               </a>
            </div>
            <div class="clear"></div>
         </div>
      </div>
   </header>
</div>
