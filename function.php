<?php

function cus_img($img)
{
    echo 'images' . '/' . $img;
}

function quote_gen()
{
    echo mt_rand(100000, 999999);
}

function get_ip_id($ip_id)
{
    echo 'assets' . '/' . $ip_id;
}

/***********************************************************
 *  Auther      : Anjali Krishnan
 *  Purpose     : Form submission of home insurance
 *  Date        : 30 Sep 2020
 **********************************************************/
include 'process.php';
$lead = new LeadGeneration();

if (isset($_REQUEST['action'])) {
    if ($_REQUEST['action'] == 'leads') {
        extract($_POST);
        $lead->processCustomerData($form_data, $submit_flag);
    }
}

?>
 