<footer>
   <div class="ftw">
      <div class="wrp cnt">
         <div class="colm oth ">
            <section id="text-4">
               <p class="ttl">Customer Services</p>
               <div class="textwidget">
                  <p><a href="https://smart-cover.co.uk/customer-reviews">Customer Reviews</a><br>
                     <a href="https://smart-cover.co.uk/key-benefits">Key Benefits </a><br>
                     <a href="https://smart-cover.co.uk/complaints">Complaints</a><br>
                     <a href="https://smart-cover.co.uk/feedback">Feedback</a><br>
                     <a href="https://smart-cover.co.uk/about-us">About Us</a><br>
                     <a href="https://smart-cover.co.uk/opt-out">Opt Out</a>
                  </p>
               </div>
            </section>
            <section id="media_image-2">
               <p class="ttl">Payment Options</p>
               <img width="250" height="50" src="https://smart-cover.co.uk/wp-content/uploads/sites/6/2017/09/card-payment-options-trans-dd-w.png" class="image wp-image-3306  attachment-full size-full" alt="" style="max-width: 100%; height: auto;">
            </section>
         </div>
         <div class="colm oth ">
            <section id="text-2">
               <p class="ttl">Help</p>
               <div class="textwidget">
                  <p><a href="https://smart-cover.co.uk/contact">Ways To Contact Us</a><br>
                     <a href="https://smart-cover.co.uk/price-match">Price Match Promise</a><br>
                     <a href="https://smart-cover.co.uk/refund-policy">Refund Policy</a>
                  </p>
                  <h6>Site News</h6>
                  <p><a href="https://smart-cover.co.uk/blog">Latest Updates</a><br>
                     <a href="https://smart-cover.co.uk/sponser">Sponsors</a><br>
                     <a href="https://smart-cover.co.uk/jobs"> Jobs</a>
                  </p>
               </div>
            </section>
            <section id="media_image-3">
               <p class="ttl">Marketing Preferences</p>
               <a href="https://smart-cover.co.uk/opt-out/"><img width="150" height="38" src="https://smart-cover.co.uk/wp-content/uploads/sites/6/2018/05/opt-out-03-150x38.png" class="image wp-image-4582  attachment-thumbnail size-thumbnail" alt="" style="max-width: 100%; height: auto;"></a>
            </section>
         </div>
         <div class="colm oth lst">
            <section id="text-3">
               <p class="ttl">Our Insurance Cover</p>
               <div class="textwidget">
                  <p><a href="https://smart-cover.co.uk/home-emergency-cover">Home Emergency</a><br>
                     <a href="https://smart-cover.co.uk/appliance-insurance">Appliance Insurance</a><br>
                     <a href="https://smart-cover.co.uk">Gadget Insurance</a><br>
                     <a href="https://smart-cover.co.uk/television-insurance">Television Insurance</a><br>
                     <a href="https://smart-cover.co.uk/satellite-insurance">Satellite Insurance</a><br>
                     <a href="https://smart-cover.co.uk">Phone Insurance</a><br>
                     <a href="https://smart-cover.co.uk/home-insurance">Home Insurance</a><br>
                     <a href="https://smart-cover.co.uk/kitchen-appliance-insurance">Home Appliance Insurance</a>
                  </p>
               </div>
            </section>
            
         </div>
         <div class="clear"></div>
      </div>
   </div>
   <div class="copy">
      <div class="wrp cnt">
         <p>
            <font color="black"><strong>Smart-Cover Insurance Services is a trading name of Smart-Cover Direct Limited, an Appointed Representative of Asurit Limited who is authorised and regulated by the Financial Conduct Authority (FCA) Smart-Cover Direct Limited Reference Number: 600428 Registered address: Ashley Court, 32 Main Street, Ashley Leicestershire LE16 8HF. The Financial Ombudsman Service (FOS) is an agency for arbitrating on unresolved complaints between regulated firms and their clients. Full details of the FOS can be found on its website at www.financial-ombudsman.org.uk.<br>
            © 2018 Smart Cover Insurance Services All Rights Reserved </strong></font><br>
            <a href="https://smart-cover.co.uk/disclaimer" target="_blank">Disclaimer</a> | ​<a href="https://smart-cover.co.uk/privacy-policy" target="_blank">Privacy </a> |​ <a href="https://smart-cover.co.uk/cookies" target="_blank">Cookies</a>
         </p>
      </div>
   </div>
</footer>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.0.6/jquery.mousewheel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
<script src="assets/he.js"></script>
<!-- BEGIN responseiQ.com widget -->
<script src="https://app.responseiq.com/widgetsrc.php?widget=78YU8MFL102U45IWBJ5&widgetrnd=Math.random();"></script>
<!-- END responseiQ.com widget -->
</body>
</html>
