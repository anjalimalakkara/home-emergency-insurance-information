$(document).ready(function(){
    $("#contact_form").validate({
        // Specify validation rules
        rules: {
          username: "required",
          email: {
            required: true
          },
          phoneNumber: {
           required: true
        }
        },
        // Specify validation error messages
        messages: {
          username    : "Please enter your name",
          phoneNumber : "Please enter your Contact  number",
          email       : "Please enter a valid email address"
        },
        submitHandler: function(form) {
          //form.submit();
         jQuery("#submit").attr("disabled", true);
          var form_data = jQuery('#contact_form').serialize();                    
          // Ajax For Posting Data To CRM
          jQuery.ajax({
              type: 'post',
              url: location.protocol + "//" + location.host + "/home-emergency-insurance-information/function.php?action=leads",
              data: {
                  form_data: form_data,
                  submit_flag: 1
              },
              success: function(result) {
                  console.log(result);
                  var myObj = JSON.parse(result);
                  jQuery('#errorNotify').html('');
                  if (myObj.status == '404') {
                      $("#submit").removeAttr("disabled");
                      jQuery('#errorNotify').html(myObj.message);
                      jQuery('#errorNotify').show();
                  }
                  if (myObj.status == '200') {
                      $( "#contact_form" ).hide(1000);
                      $( "#thank_you" ).show( 1000);
                      jQuery('#contact_form').trigger('reset');
                  }
              }
          });
        }
      });

      jQuery.validator.addMethod("username", function(uname) {
        var pattern = /^[a-z][a-z\s]*$/i;
        return pattern.test(uname);
        }, "Starting Space is not allowed in Name");


      jQuery.validator.addMethod("email", function(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
        }, "Please enter a valid email address");


      jQuery.validator.addMethod("phoneNumber", function(phone_number) {
        isNumeric(phone_number);
        document.field.phoneNumber.focus();
        document.field.phoneNumber.value="";
      }, "Please enter a valid Contact  number");

      /*jQuery.validator.addMethod("phone", function(phone_number, element) {
        var filter  = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        return filter.test(phone_number);
    }, "Please specify a valid phone number");*/


  /*********************************************************************************************************************
   *  Project Name		  : Home Emergency Insurance 
      Program name		  : Home Emergency Insurance hetemplate.php file form  validation
      Program function	: Form data transfered to CRM
      Created Date  		: 30 Sep 2020
      Author				    : Anjali Krishnan
    
      Update History
      -------------------------------------------------------------------
      Date       	By 				  		short desc. of what updated 
    
      -------------------------------------------------------------------
  * 
  * 
  ******************************************************************************************************************** */

  /**************** Phone number validation *************************/

  function isNumeric(val, cntry=0)
  {
    var nalt=document.getElementById('phoneNumber_error');
  if(val!="")
  {
      /****** 
       *  +XX-XXXX-XXXX
          +XX.XXXX.XXXX
          +XX XXXX XXXX
      * ****** below regx allows this validation
      var numericExpression = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
--------------------------------------------------------------------------------------
     for (123) 456 7899
      (123).456.7899
      (123)-456-7899
      123-456-7899
      123 456 7899
      1234567899
      var numericExpression = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
--------------------------------------------------------------------------------------


      **/

  
      var numericExpression = /^[0-9]+$/; // number only
      //var numericExpression = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
        if(val.match(numericExpression))
        {
            nalt.innerHTML="";
            return true;
        }        
        else{
            nalt.innerHTML="<font color='red'> Invalid Contact  Number</font>";
            /*document.field.phoneNumber.focus();
            document.field.phoneNumber.value="";*/
            return false;
        }
    }
    else if(val.length==0)  {
        nalt.innerHTML="Enter Contact No.";     
        return true;
    }
  }



  $("#username").focusout(function() { 
    var form_data = jQuery('#contact_form').serialize();
    var submit_flag = 0;
    var uname = $("#username").val();
    var pattern = /^[a-z][a-z\s]*$/i;
    if(uname){
        if(uname.charAt(0)==' ') {
          document.getElementById('username_error').innerHTML= "Starting Space is not allowed in Name";
          return false;              
        }
        if(!uname.match(pattern)){
          document.getElementById('username_error').innerHTML= "Enter a Valid Name";
          return false; 
        }
        else{
          document.getElementById('username_error').innerHTML= "";
          submitData(form_data, submit_flag); 
        } 
    }
  });

  $("#email").focusout(function() { 
    var form_data = jQuery('#contact_form').serialize();
    var submit_flag = 0;
    var uemail = $("#email").val();
    var emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(uemail){
        if(uemail.charAt(0)==' ') {
          document.getElementById('email_error').innerHTML= "Starting and Ending Spaces are not allowed in Email";
          return false;             
        }
        if(!uemail.match(emailPattern)){
          document.getElementById('email_error').innerHTML= "Please enter a valid email address";
          return false; 
        }
        else{
          document.getElementById('email_error').innerHTML= "";
          submitData(form_data, submit_flag); 
        } 
    }
  });

  $("#phoneNumber").focusout(function() { 
    var form_data = jQuery('#contact_form').serialize();
    var submit_flag = 0;
    var phn = ($.trim(jQuery("#phoneNumber").val()).replace(/\s/g, ''));
    /*var num = phn.replace(/^[+]/g, "");
    var matchesnumber= num.replace(/^(0044|044|44|00|0)/, "");*/
    var reg = /^[0-9]+$/;
    if(phn){
        if(!reg.test(phn) ) {   
            document.getElementById('phoneNumber_error').innerHTML= "Enter a valid phone number";
            return false;          
        }else{
          document.getElementById('phoneNumber_error').innerHTML= "";
          submitData(form_data, submit_flag);
        }
    }
  });

  //AJAX CALL
  function submitData(form_data, submit_flag)
  {
    console.log(form_data);
      jQuery.ajax({
          type: 'post',
          url: location.protocol + "//" + location.host + "/home-emergency-insurance-information/function.php?action=leads",
          data: {
              form_data: form_data,
              submit_flag : submit_flag,
          },
          success: function(result) {
              console.log("##########");
              console.log(result);
              console.log("##########");
              var myObj = JSON.parse(result);
              jQuery('#errorNotify').html('');
              if (myObj.status == '404') {
                jQuery('#errorNotify').html(myObj.message);
                jQuery('#errorNotify').show();
            }
          }
      });
  }
  

});
