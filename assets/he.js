 // for accordian arrow change and color chmage when selecting accordian
  jQuery('.panel-heading a')
            .click(function() {
            jQuery(this)
            .find(jQuery(".more-less"))
            .toggleClass("fa-chevron-circle-down fa-chevron-circle-up");
            jQuery('.panel-heading')
            .removeClass('active');

            if ( !jQuery(this)
            .closest('.panel')
            .find('.panel-collapse')
            .hasClass('in'))
            jQuery(this)
            .parents('.panel-heading')
            .addClass('active');
    });
// selecting price from 0 to 50
    jQuery('.excess_calculator span')
            .click(function() {
            jQuery('[excess_atr="'+jQuery(this)
            .parent().attr('excess_atr')+'"] span')
            .removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.track_planname_status')
            .val(jQuery(this)
            .parent().attr('excess_atr'));
             jQuery('.track_excess_status').val(jQuery(this)
            .attr('check_excess'));   
    });

    // open the pdf dialog box inside a fancy box

    // jQuery("a.light_box_popup").fancybox({
    //     width: '80%',
    //     height: '80%',
    //     autoSize: false,
    //     type: 'iframe',
    //     iframe: {
    //         preload: false
    //     }
    // }); 

 
    $(document).ready(function() {
        // set value for each plans   
        // $('.standard-block').hide(); 
        $('#togg_checkboxes').prop('checked',false);
        
            var prices = {
                'he_mn_basic_0'             : '7.99',
                'he_mn_basic_25'            : '5.99',

                'he_an_basic_0'             : '79.00',
                'he_an_basic_25'            : '59.00',
               
                'he_mn_standard_0'          : '9.99',
                'he_mn_standard_25'         : '7.99',
                'he_mn_standard_39'         : '6.99',

                'he_an_standard_0'          : '99.00',
                'he_an_standard_25'         : '79.00',
                'he_an_standard_39'         : '69.00',

                'he_mn_essential_0'         : '11.99',
                'he_mn_essential_25'        : '9.99',
                'he_mn_essential_50'        : '7.99',

                'he_an_essential_0'         : '119.00',
                'he_an_essential_25'        : '99.00',
                'he_an_essential_50'        : '79.00',

                'he_mn_premium_0'           : '14.99',
                'he_mn_premium_25'          : '13.49',
                'he_mn_premium_50'          : '11.99',

                'he_an_premium_0'           : '149.00',
                'he_an_premium_25'          : '134.00',
                'he_an_premium_50'          : '119.00',

                'he_mn_standard_0_boiler'   : '15.99',
                'he_mn_standard_25_boiler'  : '13.99',
                'he_mn_standard_39_boiler'  : '12.99',

                'he_an_standard_0_boiler'   : '159.00',
                'he_an_standard_25_boiler'  : '139.00',
                'he_an_standard_39_boiler'  : '129.00',

                'he_mn_essential_0_boiler'  : '17.99',
                'he_mn_essential_25_boiler' : '15.99',
                'he_mn_essential_50_boiler' : '13.99',

                'he_an_essential_0_boiler'  : '179.00',
                'he_an_essential_25_boiler' : '159.00',
                'he_an_essential_50_boiler' : '139.00',

                'he_mn_premium_0_boiler'    : '24.99',
                'he_mn_premium_25_boiler'   : '22.49',
                'he_mn_premium_50_boiler'   : '19.99',

                'he_an_premium_0_boiler'    : '249.00',
                'he_an_premium_25_boiler'   : '224.00',
                'he_an_premium_50_boiler'   : '199.00'
            }

            // setting all variables
            var plans = [
                'basic',
                'standard',
                'essential',
                'premium'
            ];

            var fload = [
                'he_mn_basic_25',
                'he_mn_standard_39',
                'he_mn_essential_50',
                'he_mn_premium_50'              
            ];

            var mobile_fload = [
                'he_mn_basic_25',
                'he_an_basic_25',
            ];

            var frequency = 'mn' 

            if(frequency == 'an')
                $('.frequency').val(1)
            else
                $('.frequency').val(0)

            fload.forEach(function(item) {
                pricechange(item);
            });

            mobile_fload.forEach(function(item) {
                pricechangeMobile(item);
                // alert(item)
            });
// for mobile
            $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
// getting the plan selected
                var sm = jQuery(this).attr('data-rel');

                if(sm == 'basic')
                {
                    var new_key = [
                    'he_mn_'+sm+'_25',
                    'he_an_'+sm+'_25',
                ];
                }

                else if(sm == 'standard')
                {
                    var new_key = [
                    'he_mn_'+sm+'_39',
                    'he_an_'+sm+'_39',
                ];
                }
                else
                {       
                    var new_key = [
                        'he_mn_'+sm+'_50',
                        'he_an_'+sm+'_50',
                    ];
                }
                // alert(new_key)

                new_key.forEach(function(item) {
                    pricechangeMobile(item);
                });
            });


            $('.excess_mobile').click(function() {
                var ss = jQuery(this).attr('rel').split('_');
                var tag_boiler = '';

                var tag_boiler = '';
                if($('input[name="boiler_'+ss[1]+'_mobile"]').prop('checked') == true){
                    tag_boiler = '_boiler';  
                }


                var new_key = [
                    'he_mn_'+ss[1]+'_'+ss[2]+tag_boiler,
                    'he_an_'+ss[1]+'_'+ss[2]+tag_boiler,
                ];

                new_key.forEach(function(item) {
                    pricechangeMobile(item);
                });

                
            });

            $('.boiler_mobile').click(function(){
                var tag = $(this).attr('name');
                var split = tag.split("_"); 
                var tag_plan = split[1];                 
                var tag_excess = jQuery('.mob_'+tag_plan+' .excess_mobile.active')
                                 .attr('rel').split('_');
                var tag_boiler = '';
                if($('input[name="boiler_'+tag_plan+'_mobile"]').prop('checked') == true)
                    tag_boiler = '_boiler';
                var new_key = [
                    'he_mn_'+tag_plan+'_'+tag_excess[2]+tag_boiler,
                    'he_an_'+tag_plan+'_'+tag_excess[2]+tag_boiler,
                ]; 
 
                new_key.forEach(function(item) {
                    pricechangeMobile(item);
                }); 

            }); 


            function pricechangeMobile(key)
            {
                var split = key.split("_");
                var pc_frequency    = split[1];
                var pc_plan         = split[2];
                var pc_excess       = split[3];
                var pc_boiler       = split[4];  
  
                var form_frequency = ''; 
                if(pc_frequency == 'mn') { 
                    form_frequency = 'N';
                }

                if(pc_frequency =='an') { 
                    form_frequency = 'Y';
                }

                var cover_types = '';

                if( pc_plan =='basic') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_250_'+pc_excess;
                    }  
                }

                if( pc_plan =='standard') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_st_250_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_st_bs_250_'+pc_excess;
                    }
                }

                if( pc_plan =='essential') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_500_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_bs_500_'+pc_excess;
                    }
                }

 
                if( pc_plan =='premium') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_1000_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_bs_1000_'+pc_excess;
                    }
                }


                var dis_price = prices[key]; 

                $('.amount_mobile.'+pc_frequency).html('&pound;'+dis_price);
                $('.excess_mobile').removeClass('active');
                $('.excess_calculator .excess_mobile[rel="excess_'+pc_plan+'_'+pc_excess+'"]').addClass('active'); 
// alert(dis_price)
                jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="PolicyPrice"]').val(dis_price);
                jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="is_annually"]').val(form_frequency);
                jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="PolicyExcess"]').val(pc_excess);
                jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="cover_type"]').val(cover_types);

                if(pc_boiler == undefined || pc_boiler == ''){

                    jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="boiler_cover_annual_service"]').val('N');

                  }

                if(pc_boiler != undefined || pc_boiler == 'boiler'){
                    jQuery('.fm_'+pc_plan+'_'+pc_frequency+ ' [name="boiler_cover_annual_service"]').val('Y');
                }

             }

            function pricechange(key)
            {
                // alert(key)
                var split = key.split("_");
                var pc_frequency    = split[1];
                var pc_plan         = split[2];
                var pc_excess       = split[3];
                var pc_boiler       = split[4];
                // alert(pc_excess) 

                var form_frequency = ''; 
                if(pc_frequency == 'mn') { 
                    form_frequency = 'N';
                }

                if(pc_frequency =='an') { 
                    form_frequency = 'Y';
                }

                var cover_types = '';

                if( pc_plan =='basic') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_250_'+pc_excess;
                    }  
                }

                if( pc_plan =='standard') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_st_250_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_st_bs_250_'+pc_excess;
                    }
                }

                if( pc_plan =='essential') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_500_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_bs_500_'+pc_excess;
                    }
                }

                if( pc_plan =='premium') { 

                    if( pc_boiler == undefined || pc_boiler == '' ) {
                        cover_types = 'he_1000_'+pc_excess;
                    }  

                    if( pc_boiler == 'boiler') {
                        cover_types = 'he_bs_1000_'+pc_excess;
                    }
                }


            // setting Annually or monthly
                var dis_freq = (pc_frequency == 'mn')?'Monthly':'Annually';
            // assigning that value in the front end
            // alert(dis_freq)
                $('.'+pc_plan+'_main .plan_freq').html(dis_freq);
            // getting the price value
                var dis_price = prices[key];
                // alert(key)
                // alert(dis_price)
            // assigning that value in the front end
                $('.'+pc_plan+'_main .amount').html('&pound;'+dis_price);

                $('input[rel="excess_'+pc_plan+'_'+pc_excess+'"]').prop('checked', true);
                
                $('input[name="boiler_'+pc_plan+'"]').prop('checked', false);
                if(pc_boiler == 'boiler' && pc_boiler != undefined)
                {
                    $('input[name="boiler_'+pc_plan+'"]').prop('checked', true);
                }
                // alert(cover_types)
                 jQuery('.fm_'+pc_plan+' [name="is_annually"]').val(form_frequency);
                 jQuery('.fm_'+pc_plan+' [name="PolicyExcess"]').val(pc_excess);
                 jQuery('.fm_'+pc_plan+' [name="PolicyPrice"]').val(dis_price);
                 jQuery('.fm_'+pc_plan+' [name="cover_type"]').val(cover_types);

                 if(pc_boiler == undefined || pc_boiler == ''){
                    jQuery('.fm_'+pc_plan+' [name="boiler_cover_annual_service"]').val('N');
                 }

                if(pc_boiler != undefined || pc_boiler == 'boiler'){
                    jQuery('.fm_'+pc_plan+' [name="boiler_cover_annual_service"]').val('Y');
                }
                
            }

            $('.excess').click(function(){
                var tag = $(this).attr('rel');
                var split = tag.split("_");
                // alert(tag)
                var tag_plan = split[1];
                var tag_excess = split[2];

                var tag_freq = 'mn';
                if($('.frequency').val() == 1)
                    tag_freq = 'an';
                else
                    tag_freq = 'mn';

                var tag_boiler = '';
                if($('input[name="boiler_'+tag_plan+'"]').prop('checked') == true)
                    tag_boiler = 'boiler';

                var key = 'he_'+tag_freq+'_'+tag_plan+'_'+tag_excess;
                if(tag_boiler != '')
                    key = key+'_boiler';

                pricechange(key);
            });

            $('.boiler').click(function(){
                var tag = $(this).attr('name');
                var split = tag.split("_"); 

                var tag_plan = split[1];
                var tag_excess = $('input[name="excess_'+tag_plan+'"]:checked').val();

                var tag_freq = 'mn';
                if($('.frequency').val() == 1)
                    tag_freq = 'an';
                else
                    tag_freq = 'mn';
                var tag_boiler = '';
                if($('input[name="boiler_'+tag_plan+'"]').prop('checked') == true)
                    tag_boiler = 'boiler';

                var key = 'he_'+tag_freq+'_'+tag_plan+'_'+tag_excess;
                if(tag_boiler != '')
                    key = key+'_boiler';
                // alert(key)
                pricechange(key);
            });

            $('.frequency').change(function() {

                    if( jQuery('#togg_checkboxes:checked').length == 1 ) {

                        $(this).val(1);
                        $('.standard-block').show();
                    }

                    if( jQuery('#togg_checkboxes:checked').length == 0 ) {

                        $(this).val(0)

                        // $('.standard-block').hide();

                    }

                var select = $(this).val();

                var tag_freq = 'mn';
                if(select == 1)
                    tag_freq = 'an';
                else
                    tag_freq = 'mn';
                // alert(plans)
                plans.forEach(function(plan) {
                    var tag_plan = plan;
                    // alert(plan)
                    // if(tag_plan == 'standard')
                    // {

                    // }
                    var tag_excess = $('input[name="excess_'+tag_plan+'"]:checked').val();
                    // alert(tag_excess)
                    var tag_boiler = '';
                    if($('input[name="boiler_'+tag_plan+'"]').prop('checked') == true)
                        tag_boiler = 'boiler';
                   
                    var key = 'he_'+tag_freq+'_'+tag_plan+'_'+tag_excess;
                    
                    var key = 'he_'+tag_freq+'_'+tag_plan+'_'+tag_excess;
                    // alert(key)
                    if(tag_boiler != '')
                        key = key+'_boiler';

                    pricechange(key);
                });
            })
            $('#togg_checkboxes').trigger('click');
        });
        $('.ht-icon-hotspot-pulse').click(function(){
            $(this).parent().toggleClass('zindexhigh');
        });
        $('.ht-icon-hotspot-close').click(function(){
            $(this).parent().parent().prev().prev().checked = false;
            $(this).parent().parent().parent().removeClass('zindexhigh');
        });
            
jQuery(window).scroll(function(){
        if (jQuery(window).scrollTop() >= 800) {
            jQuery('.divfxdtop').addClass('pposfixed');
            jQuery('.hetempfooter').addClass('def');
        }
        else{
            jQuery('.divfxdtop').removeClass('pposfixed');
            jQuery('.hetempfooter').removeClass('def');

        }
});
$('#ftrclose').click(function (e) { 
    e.preventDefault();
    $(this).parent().parent().parent().fadeOut();
});
$(document).ready(function () {
    $("#carousel").owlCarousel({
        autoplay: true,
        dots:true,
        loop:true,
        margin: 20,
        responsiveClass: true,
        autoHeight: true,
        smartSpeed: 1200,
        nav: false,
        autoplayTimeout:6000,
        responsive: {
          0: {
            items: 1
          },
      
          600: {
            items: 1
          },
      
          1024: {
            items: 1
          },
      
          1366: {
            items: 1
          }
        }
      });
});