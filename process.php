<?php
/*********************************************************************************************************************
   *  Project Name		  : Home Emergency Insurance 
      Program name		  : Home Emergency Insurance hetemplate.php file form  validation
      Program function	  : Form data transfered to CRM
      Created Date  	  : 30 Sep 2020
      Author			  : Anjali Krishnan
    
      Update History
      -------------------------------------------------------------------
      Date       	By 				  		short desc. of what updated 
    
      -------------------------------------------------------------------
  * 
  * 
  ******************************************************************************************************************** */


class LeadGeneration
{
    // const APIURL = 'https://www.repair-network.co.uk/repair_network_crm/api/addQuickEnquries';
    // const APIURL = 'https://smartapps-yarabltd.co.uk/repair_network_crm/api/addQuickEnquries';
    //const APIURL = 'http://192.168.1.5/repair_network_crm/api/addQuickEnquries';
    const APIURL = 'http://192.168.1.104/repair_network_crm/api/addQuickEnquries';

                
    public function encrypt($data)
    {
        $method = 'AES-256-CBC';
        
        $key       = hash('sha256', 'qJB0rGtIn5UB1fa_yyefyCp');
        $ivSize    = openssl_cipher_iv_length($method);
        $iv        = openssl_random_pseudo_bytes($ivSize);
        $encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
        $encrypted = base64_encode($iv . $encrypted);
        return $encrypted;
    }
    
    public function decrypt($data)
    {
        $method = 'AES-256-CBC';
        
        $key    = hash('sha256', 'qJB0rGtIn5UB1fa_yyefyCp');
        $data   = base64_decode($data);
        $ivSize = openssl_cipher_iv_length($method);
        $iv     = substr($data, 0, $ivSize);
        $data   = openssl_decrypt(substr($data, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);
        return $data;
    }
    
    public function processCustomerData($data, $submit_flag)
    {
        session_start();

        $error       = '';
        $throw_error = '';
        parse_str($data, $customer_data);
        
        if (ctype_alpha(str_replace(' ', '', $customer_data['username'])) === false && $customer_data['username'] != '') {
            $error .= '<p class="error">Name is Invalid</p>';
        }

        if (!filter_var($customer_data['email'], FILTER_VALIDATE_EMAIL) && $customer_data['email'] != '') {
            $error .= '<p class="error">Email is Invalid</p>';
        }

        if ($customer_data['phoneNumber'] != '' && !is_numeric($customer_data['phoneNumber']) ) {
            $error .= '<p class="error">Phone number is invalid</p>';
        }

        if ($customer_data['contact'] == '') {
             $error .= '<p class="error">Consent to Contact is required</p>';
        }

        $customer_name = trim($customer_data['username']); 
         
        if (empty($error)) {
            if (isset($_SESSION['reference_id'])) {
                $reference_id = $_SESSION['reference_id'];
            } else {    
                $reference_id =  time().mt_rand(10000000, 99999999);
                $_SESSION['reference_id'] = $reference_id;
            }

            $data = array(
                'campaign' => "Home Emergency Insurance",
                'user_name' => isset($customer_name) ? $customer_name : '',
                'contact_number' => isset($customer_data['phoneNumber']) ? trim($customer_data['phoneNumber']) : '',
                'user_email' => isset($customer_data['email']) ? trim($customer_data['email']) : '',
                'consent_to_contact' => isset($customer_data['contact']) ? trim($customer_data['contact']) : '',
                'reference_id' => isset($reference_id) ? $reference_id : ''
            );
            
            $this->CurlWrapper($this->encrypt(json_encode($data)), self::APIURL);

            if ($submit_flag == 1) {
                session_unset();
            }
            
            $throw_error = array(               
                'status' => 200,
                'message' => "<p>You have successfully completed the process</p>"      
            );
            
            echo json_encode($throw_error);      
            
        } else {
            
            $throw_error = array(   
                'status' => 404,
                'message' => $error
            );
            
            echo json_encode($throw_error);
        }
    }
    
    public function CurlWrapper($data, $url)
    {
        $ch         = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => array(
                'data' => $data
            )
        );
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
?>