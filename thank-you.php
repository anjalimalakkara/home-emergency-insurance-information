<!DOCTYPE html>
<html lang="en">
<head>
  <title>Thank You</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/he.css">
  <!-- OWL -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172941006-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-172941006-1');
</script>
</head>

<body>
  <script>
  fbq('track', 'Lead');
</script>
  <div class="page_wrap">
  <header>
        <div class="container">
            <div class="row">
                <div class="col-12">
                <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a class="navbar-brand  d-md-none" href="#">
                <img src="images/logo.png" class="img-fluid logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="tel:03333447988">CALL: 03333447988</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">APPLY NOW</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">RENEW POLICY</a>
                </li>    
                <li class="nav-item">
                    <a class="nav-link" href="#">CLAIM NOW</a>
                </li>   
                </ul>
            </div>  
            </nav>
                </div>
            </div>
        </div>
   </header>
   <section class="topsec d-none d-md-block">
       <div class="container">
           <div class="row">
               <div class="col-lg-2 d-md-block d-none">
                   <a href="#">
                       <img src="images/logo.png" class="img-fluid logo">
                   </a>
               </div>
               <div class="col-lg-5 text-center">
                    <h2 class="mb-0">AWARD WINNING</h2>
                    <h4 class="mb-0">INSURANCE PROVIDER</h4>
               </div>
               <div class="col-lg-5 text-center">
                    <h2 class="mb-0">SKY Insurance Cover</h2>
                    <h6 class="mb-0">Protect you from unexpected events</h6>
                    <p class="mb-0">Multiple Devices & Sky TV Under One Policy.</p>
                </div>
           </div>
       </div>
   </section>
    <section class="thnk_sec pt-5 pb-5 thank text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
          <svg version="1.1" id="Capa_1" class="mb-3" xmlns="http://www.w3.org/2000/svg" width="120" height="120" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 511.993 511.993" style="enable-background:new 0 0 511.993 511.993;" xml:space="preserve">
<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="69.654" y1="448.2119" x2="416.1364" y2="101.7296" gradientTransform="matrix(1.0449 0 0 -1.0449 2.1993 543.3086)">
	<stop  offset="0" style="stop-color:#00E8F2"/>
	<stop  offset="1" style="stop-color:#006EF5"/>
</linearGradient>
<circle style="fill:url(#SVGID_1_);" cx="255.996" cy="255.996" r="255.996"/>
<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="49.8952" y1="274.9707" x2="435.8952" y2="274.9707" gradientTransform="matrix(1.0449 0 0 -1.0449 2.1993 543.3086)">
	<stop  offset="0" style="stop-color:#FFFFFF"/>
	<stop  offset="0.31" style="stop-color:#FCFCFC"/>
	<stop  offset="0.573" style="stop-color:#F2F1F3"/>
	<stop  offset="0.818" style="stop-color:#E1E0E3"/>
	<stop  offset="1" style="stop-color:#CFCDD3"/>
</linearGradient>
<circle style="fill:url(#SVGID_2_);" cx="255.996" cy="255.996" r="201.662"/>
<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="180.3917" y1="353.4022" x2="300.8062" y2="232.9877" gradientTransform="matrix(1.0449 0 0 -1.0449 2.1993 543.3086)">
	<stop  offset="0" style="stop-color:#00E8F2"/>
	<stop  offset="1" style="stop-color:#006EF5"/>
</linearGradient>
<polygon style="fill:url(#SVGID_3_);" points="214.251,402.119 88.433,276.301 134.239,230.493 214.251,310.503 372.954,151.801 
	418.761,197.608 "/>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
          </div>
          <div class="col-md-12">
            <div class="thnk_res">
              <h2>Thank You</h2>
              <p>Thank you for registering your interest.<br> We will get back to you as soon as possible.
              </p>
            </div>
          </div>
        </div>
      </div>
    <!-- wrapper div -->
  </div>
  <footer>
        <div class="container">
            <div class="footer-sec">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>Our Insurance Cover</h4>
                        <ul>
                            <li>
                                <a href="https://smart-cover.co.uk/home-emergency-cover-protection/">
                           Home Emergency
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/appliance-insurance/">
                           Appliance Insurance
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/gadgets-insurance/">
                           Gadget Insurance 
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/television-insurance">
                           Television Insurance
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/satellite-insurance/">
                           Satellite Insurance
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/home-insurance">
                           Home Insurance
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/kitchen-appliance-insurance/">
                           Home Appliance Insurance
                           </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <h4>Help</h4>
                        <ul>
                            <li>
                                <a href="https://smart-cover.co.uk/contact/">
                           Contact Us
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/price-match/">
                           Price Match Promise
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/refund-policy/">
                           Refund Policy
                           </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <h4>Customer Services</h4>
                        <ul>
                            <li>
                                <a href="https://smart-cover.co.uk/customer-reviews/">
                           Customer Reviews
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/key-benefits/">
                           Key Benefits
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/complaints/">
                           Complaints
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/feedback/">
                           Feedback
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/about-us/">
                           About Us
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/opt-out/">
                           Opt-Out
                           </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <h4>Site News</h4>
                        <ul>
                            <li>
                                <a href="https://smart-cover.co.uk/blog/">
                           Latest Update
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/sponser/">
                           Sponsors
                           </a>
                            </li>
                            <li>
                                <a href="https://smart-cover.co.uk/jobs/">
                           Jobs
                           </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h4>Payment Options</h4>
                        <img src="images/payment.png" class="img-fluid">
                        <br><br>
                        <h4>
                            Follow Us
                        </h4>
                        <div class="footer-social-links py-3">
                            <div class="social-icon-wrap fb-wrap"><a href="#" class="social-icon" target="_blank"><i class="fab fa-facebook-f fb-color"></i></a></div>
                            <div class="social-icon-wrap tw-wrap"><a href="https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fsmart-cover.co.uk%2F&ref_src=twsrc%5Etfw&region=follow_link&screen_name=smartcoveruk&tw_p=followbutton" class="social-icon" target="_blank"><i class="fab fa-twitter tw-color"></i></a></div>
                            <div class="social-icon-wrap gp-wrap"><a href="#" class="social-icon" target="_blank"><i class="fab fa-google-plus-g gp-color"></i></a></div>
                            <div class="social-icon-wrap ln-wrap"><a href="#" class="social-icon" target="_blank"><i class="fab fa-linkedin-in ln-color"></i></a></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 text-center">
                        <p>
                            This website is intended for customers based in the UK and is therefore subject to the UK regulatory regime(s) Smart-Cover Insurance Services is a trading name of Smart-Cover Direct Limited, an Appointed Representative of Asurit Limited who is authorised
                            and regulated by the Financial Conduct Authority (FCA), this can be verified by visiting the FCA’s register. Smart-Cover Direct Limited Reference Number: 600428 Registered address: Ashley Court, 32 Main Street, Ashley Leicestershire
                            LE16 8HF. The Financial Ombudsman Service (FOS) is an agency for arbitrating on unresolved complaints between regulated firms and their clients. Full details of the FOS can be found on its website at www.financial-ombudsman.org.uk.
                            In addition to this, the EU’s Online Dispute Resolution platform may be found on their website at https://ec.europa.eu/consumers/odr.
                        </p>
                    </div>
                    <div class="col-12 text-center">
                        <span>
                     <a href="https://smart-cover.co.uk/disclaimer/" class="dt-br1"> 
                     Disclaimer
                     </a>
                     <a href="https://smart-cover.co.uk/privacy-policy/" class="dt-br1"> 
                     Privacy
                     
                     </a><a href="https://smart-cover.co.uk/cookies/"> 
                     Cookies
                     </a>
                     </span>
                    </div>
                    <div class="col-12 text-center">
                        <p class="pt-5">© 2020 Smart Cover Insurance Services All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/parallax.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    function checkValidation()
           {
              //alert("a");
              var name = jQuery("#nme").val();
              var email = jQuery("#eml").val();
              var phone = jQuery("#phn").val();
              var optradio = jQuery('input:radio[name=optradio]:checked').val();
              // if (jQuery("#ys").val() == 'Yes') {
              //   var optradio = jQuery("#ys").val();       
              // }
              // if (jQuery("#nope").val() == 'No') {
              //   var optradio = jQuery("#nope").val();       
              // }
             
              var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
              var phone_pattern = /^[\d\s+-]+$/;
              
           
            if(name == '' )
              {          
                alert("Enter your name");
                jQuery("#nme").focus();
                return false;              
              }
            else if(phone == '')
              {
                //alert("aa");
                alert("Enter your 10 digit phone number");
                jQuery("#phn").focus();
                return false;              
              }
            else if(!phone_pattern.test(phone))
              {
                alert("Invalid phone number");
                jQuery("#phn").focus();
                return false;          
              }
            else if(phone.length < 10 || phone.length > 10 )
              {   
                alert("Invalid phone number");
                jQuery("#phn").focus();
                return false;          
              }
            else if(email == '')
              {
                //alert("aa");
                alert("Enter your email id");
                jQuery("#eml").focus();
                return false;              
              }
            else if(!email_pattern.test(email))
              {
                alert("Invalid email id");
                jQuery("#eml").focus();
                return false;          
              }
            else if($('input[name="optradio"]:checked').length == 0)
              {
                //alert("aa");
                alert("Select Consent to Contact");
                jQuery("#ys").focus();
                return false;              
              }
              else
                {
                   jQuery("#ban_btn").attr("disabled", true);
                     $.ajax({
                         url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSegaZ-ZIsAKsg7lZusf6eVP7E4Wd3PWQqDoyXMqs3u6woQ6BQ/formResponse",
                          data: {
                           "entry.190435932": name,
                           "entry.61815558": phone,
                           "entry.1410031939": email,
                           "entry.1194089773": optradio,
                         },
                         type: "POST",
                         crossDomain: true,
                         dataType: "json",
                         statusCode: {
                           0: function() {
                                console.log('Failure');
                                window.location.href='thank-you.html';
                           },
                           200: function() {
                                console.log('Success');
                                window.location.href='thank-you.html';
                           }
                         },
                          // success: success
                      });                    
                  
                }
             }
    
          function checkValidationfoo()
           {
              //alert("a");
              var name_foo = jQuery("#nme1").val();
              var email_foo = jQuery("#eml1").val();
              var phone_foo = jQuery("#phn1").val();
              var optradio_foo = jQuery('input:radio[name=optradio]:checked').val();
              // if (jQuery("#ys").val() == 'Yes') {
              //   var optradio = jQuery("#ys").val();       
              // }
              // if (jQuery("#nope").val() == 'No') {
              //   var optradio = jQuery("#nope").val();       
              // }
             
              var email_pattern_foo = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
              var phone_pattern_foo = /^[\d\s+-]+$/;
              
           
            if(name_foo == '' )
              {          
                alert("Enter your name");
                jQuery("#nme1").focus();
                return false;              
              }
            else if(phone_foo == '')
              {
                //alert("aa");
                alert("Enter your 10 digit phone number");
                jQuery("#phn1").focus();
                return false;              
              }
            else if(!phone_pattern_foo.test(phone_foo))
              {
                alert("Invalid phone number");
                jQuery("#phn1").focus();
                return false;          
              }
            else if(phone_foo.length < 10 || phone_foo.length > 10 )
              {   
                alert("Invalid phone number");
                jQuery("#phn1").focus();
                return false;          
              }
            else if(email_foo == '')
              {
                //alert("aa");
                alert("Enter your email id");
                jQuery("#eml1").focus();
                return false;              
              }
            else if(!email_pattern_foo.test(email_foo))
              {
                alert("Invalid email id");
                jQuery("#eml1").focus();
                return false;          
              }
            else if($('input[name="optradio"]:checked').length == 0)
              {
                //alert("aa");
                alert("Select Consent to Contact");
                jQuery("#ys1").focus();
                return false;              
              }
              else
                {
                   jQuery("#foo_btn").attr("disabled", true);
                     $.ajax({
                         url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSegaZ-ZIsAKsg7lZusf6eVP7E4Wd3PWQqDoyXMqs3u6woQ6BQ/formResponse",
                          data: {
                           "entry.190435932": name_foo,
                           "entry.61815558": phone_foo,
                           "entry.1410031939": email_foo,
                           "entry.1194089773": optradio_foo,
                         },
                         type: "POST",
                         crossDomain: true,
                         dataType: "json",
                         statusCode: {
                           0: function() {
                                console.log('Failure');
                                window.location.href='thank-you.html';
                           },
                           200: function() {
                                console.log('Success');
                                window.location.href='thank-you.html';
                           }
                         },
                          // success: success
                      });                    
                  
                }
             }
  </script>
  <script>
    AOS.init({
               easing: '',
               duration: 1000
             });
  </script>
  <!-- <script>
         $(window).scroll(function () {
             var scroll = $(window).scrollTop();
         
             if (scroll >= 100) {
                 $(".page_hdr").addClass("page_hdr_fixed");
             } else {
                 $(".page_hdr").removeClass("page_hdr_fixed");
             }
         });
         </script> -->
  <script type="text/javascript">
    $('#carousel_a').owlCarousel({
             loop:true,
             margin:25,
             nav:true,
             dots:false,
             responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:4
                 }
             }
             })
  </script>
  <script type="text/javascript">
    $('#carousel_b').owlCarousel({
             loop:true,
             margin:25,
             nav:true,
             dots:false,
             responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:4
                 }
             }
             })
  </script>
  <script type="text/javascript">
    $('#carousel_c').owlCarousel({
             loop:true,
             margin:25,
             nav:true,
             dots:false,
             responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:4
                 }
             }
             })
  </script>
  <script type="text/javascript">
    $(".gt_cl_back").click(function() {
              $('html, body').animate({
                scrollTop: $(".bnr_frm").offset().top
              }, 500);
             });
    
              $(".scn_f .cro_bot").click(function() {
              $('html, body').animate({
                scrollTop: $(".bnr_frm").offset().top
              }, 500);
             });
    
               $(".scn_d .cro_bot").click(function() {
              $('html, body').animate({
                scrollTop: $(".bnr_frm").offset().top
              }, 500);
             });
  </script>
</body>

</html>