<div class="he_page_template">

<section id="image_banner">
    <div class="container">
       <div class="row">
          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
             <div class="content_he">
                <h1>Home Emergency Cover - 24/7 - 365 Days</h1>
                <!-- <h4>Protection plans for heating, plumbing, electrics, boiler and much more</h4> -->
                <div class="owl-slider">
                   <div class="owl-carousel" id="carousel">
                      <div class="item">
                         <h4>Protection plans for heating, plumbing, electrics, boiler and much more</h4>ap
                      </div>
                      <div class="item">
                         <h4>Avoid Expensive Emergency Repair Costs With Our 5 Star Rated Home Emergency Insurance</h4>
                      </div>
                      <div class="item">
                         <h4>Claim limit up to £5000 per year, Flexible Plans Available for Your Requirements</h4>
                      </div>
                      <div class="item">
                         <h4>Rapid Emergency Reponse, UK Customer Service & Nationwide Engineers</h4>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </section>
<section class="aw">
    <div class="container text-center">
        <div class="row">
        
            <div class="col-md-3">
                <div class="rvio">
                    <script src="https://widget.reviews.co.uk/badge-ribbon/dist.js"></script>
                         <div id="badge-ribbon"></div>
                         <script>
                                 reviewsBadgeRibbon("badge-ribbon", {
                                     store: "smart-cover-insurance1",
                                     mono: "",
                                     size: "small",
                                 });
                         </script>
                 </div>
            </div>
            <div class="col-md-5">
                 <div class="trust">
                     <!-- TrustBox widget - Micro Star -->
                    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="5746ab150000ff00058d6354" data-style-height="24px" data-style-width="100%" data-theme="light">
                      <a href="https://uk.trustpilot.com/review/smart-cover.co.uk" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                 </div>
            </div>
            <div class="col-md-2" style="display:flex; justify-content: center;">
                <img src="images/money-fact-logo.jpg" class="img-responsive" width="120">
                
            </div>
        </div>
    </div>
</section>
    
    <section class="imgmapsec">
        <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                  <div class="imglft">
                      <p style="font-size:28px; font-weight:700px">A STEP UP FOR WINTER</p>
                      <p style="padding-top:10px">Boiler, heating & plumbing care with a</p>
                      <h1>
                        Price Match Promise
                      </h1>
                      <h6>
                      Smart Cover offers a price match promise and will match a competing offer if you find the same level of cover elsewhere. <br>
                      Home emergency cover helps cover the cost of call-out fees and emergency repairs for things like central heating, boiler, plumbing, etc...
                      </h6>
                      <div class="row">
                          <div class="col-xs-12">
                              <form method="post" target="_blank" action="https://smartcover247.co.uk/gocomparequote/jump/go-compare/home-emergency" class="fm_premium">
                                    <input type="hidden" class="he_premium_mn_viewz" name="PolicyPrice" value="19.99">
                                    <input type="hidden" name="Source" value="QuoteZone">
                                    <input type="hidden" name="boiler_cover_annual_service" value="Y">
                                    <input type="hidden" name="PolicyExcess" value="50">
                                    <input type="hidden" name="cover_type" value="he_bs_1000_50">
                                    <input type="hidden" name="is_annually" value="N">
                                    <input type="hidden" name="quote_id" value="971054">
                                    <div>
                                        <button type="submit" class="applbtn">APPLY NOW</button>
                                    </div>
                                </form>
                        </div>
                       
                         
                   
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </section>
   
    <section class="wibnr">
        <!-- <img src="images/he banner-new.jpg" class="img-responsive"> -->    
        <div class="wibnr_content">
            <div class="col-w-4">
                <div class="contact_form">
                                
                <div class="form-group" id = "thank_you" style = "display:none;">
                    <label><h2>Thank You</h2>
                    <p>Thank you for registering.<br> We will get back to you as soon as possible.</p>
                    </label>
                </div>

                    <form id="contact_form" >
                        <div class="form-group">
                            <label><b>Name</b><span class="req">*</span></label>
                            <input type="text" class="form-control" name="username" id = "username" placeholder="Enter your name" required>
                            <label class = "custError" id = "username_error" ></label>
                        </div>
                        <div class="form-group">
                            <label><b>Email</b><span class="req">*</span></label>
                            <input type="text" class="form-control" name="email" id ="email" placeholder="Enter your email" required>
                            <label class = "custError" id = "email_error" ></label>
                        </div>
                        <div class="form-group">
                            <label><b>Contact No</b><span class="req">*</span></label>
                            <input type="tel" class="form-control" name="phoneNumber" id = "phoneNumber"   placeholder="Enter your phone number" required>
                            <label class = "custError" id = "phoneNumber_error" ></label>
                        </div>
                        <div class="form-group">
                        <label><b>Consent to contact</b></label>
                        <div class="group">
                            <label><input type="radio" name="contact" value="yes" checked>Yes, I would like to hear about the offer and services</label>
                        </div>
                        <div class="group">
                            <label><input type="radio" name="contact" value="no">No thanks, I dont want to hear about the offers and services</label>
                        </div>
                        <label class = "custError" id = "errorNotify" style = "position: relative;" ></label>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn" id = "submit" value="SUBMIT">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-w-6">
                <img src="images/he banner-new.jpg" height="500px"/>
                <!-- <div class="bg_img" style="background:url('images/he banner-new.jpg') no-repeat left center/cover"></div> -->
                <div class="col-md-12 text-center">
                <h3 style="color:#fff">Prices mentioned above is an independent in-house data source for estimating costs.</h3>
            </div>
            </div>
        </div>
    </section>
    <section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Is it worth paying for home emergency cover?</h1>
            </div>
        </div>
    </div>
</section>
    <section class="cnt">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>There is an increasing trend for insurers to offer home emergency cover and any homeowner should question whether they need it.</p>

<p>Home emergency cover can be a separate policy or an add-on to your existing home and contents insurance. It provides insurance for items not covered in your general insurance which covers your building, contents, and garden sheds. 
<section class="banner">
<h2>Designed to protect your home emergencies against the expensive cost of breakdown</h2>
</section>
</p>
<div class="row">
    <div class="col-md-6">
        <p><strong>What is Home Emergency Insurance?</strong></p>
<p>Home emergency insurance typically covers central heating, boiler breakdown, plumbing, electrics, and pipework. Unprecedented emergency issues with these can cause damage and could impact the health of the people living in your house. 
</p><p><strong>What Counts as an Emergency?</strong></p>

•	Boiler breakdown<br>
•	No heating<br>
•	No hot water<br>
•	Blocked drains<br>
•	Sudden emergency power outage<br>
•	Broken doors, windows, or locks<br>
•	Rodent infestation<br>

<p>
Home emergency insurance doesn’t cover wear and tear – and this includes damp and rot, and also some policies do not cover boilers over a certain age, so check the policy for this condition. 
</p>
    </div>
    <div class="col-md-6">
        <img src="images/home-emergency-insurance-information-1.png" class="img-responsive">
    </div>
</div>
<p><strong>What Can I Claim For?</strong></p>
Policies will vary between insurers and also if your policy is a separate policy or an add-on/part of your house insurance, so it is important you know what you are paying for. 
Generally, most policies will offer the following:</p>
<strong>• 24-hour emergency helpline</strong><br>
<strong>• Access to approved tradesmen who will fix the problem</strong><br>
<strong>• Cover for parts and labour – may be capped or subject to an excess.</strong><br>
</p>
<p>
The policy will cover the fix/repair but any damage caused by the problem will be covered by your general household insurance – for example, if you had a flood and your flooring was ruined, you would have to claim for a new carpet on your house insurance not on your home emergency cover. 
This point is especially important to note for landlords who typically do not have contents insurance. Another important thing to know is that for home emergency cover to be valid, your home should be in good, maintained order. 
</p>
<p>
<strong>Benefits and Reasons to Have Home Emergency Cover</strong><br>
An emergency is exactly that. An emergency prevents you carrying on life in your home as normal. It is not something you can ignore, work around, or manage by “getting by”. The emergency can compromise the integrity of your home, can impinge on safety and security, and/or pose a risk to your health. 
</p><p>
    <div class="row">
        <div class="col-md-6">
            
<h4><strong><i>It saves money</i></strong></h4>
Because an emergency needs immediate attention, cost is a major factor. Even tradesmen who advertise that they will respond to emergency calls will charge a premium for an urgent callout. The cost of an emergency callout can be eye-wateringly high and then there are the job costs on top of that. With home emergency cover, your insurance pays.
Your annual premium is likely to be a fraction of the cost of a major emergency, so payback is excellent. Usually, you can claim upto £5000 a year 
(to a limit of £1000 per claim) as necessary on home emergency cover and it will not affect your no-claims bonus on your household insurance. 
</p><p>
<h4><strong><i>It saves time</i></strong></h4>
The 24-hour helpline means your problem receives attention as soon as possible. A speedy response can prevent further damage. If you don’t have emergency home cover, you first have to find a tradesman/technician and that tradesman has to be available right away. It can take some time to search online or elsewhere to find a company and ring around to get some quotes. This urgency can induce a state of panic which puts you at the risk of engaging a “dodgy” tradesman who does an inferior job because you don’t have time to carry out a thorough check on them. 
</p>
<p>
<h4><strong><i>Peace of Mind</i></strong></h4>
With home emergency cover you know problems will be dealt with as soon as possible, enabling home life to get back to normal quickly. You have the peace of mind knowing that you won’t have to find a significant sum of money quickly. You also know that your emergency will be attended to by a qualified, registered, and test-proven tradesman. 
With the increasing amount of time we spend at home and the greater incidence of adverse weather conditions, emergencies happen more often. Home emergency insurance makes good sense. 
 </p>
            
        </div>
        <div class="col-md-6">
            <img src="images/home-emergency-insurance-information-2.png" class="img-responsive">
        </div>
    </div>
 </div>
            </div>
        </div>
    </section>
<!-- TrustBox widget - Slider -->
<div class="trustpilot-widget" data-locale="en-GB" data-template-id="54ad5defc6454f065c28af8b" data-businessunit-id="5746ab150000ff00058d6354" data-style-height="240px" data-style-width="100%" data-theme="light" data-stars="1,2,3,4,5" data-review-languages="en">
    <a href="https://uk.trustpilot.com/review/smart-cover.co.uk" target="_blank" rel="noopener">Trustpilot</a>
  </div>
  <!-- End TrustBox widget -->